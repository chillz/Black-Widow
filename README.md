# Black Widow

### Teensy script for exfiltration of network passwords on a Windows 10+ computer.

Computers unconditionally trust user inputs. This trust can be easily abused by plugging in a device, which acts like a keyboard and enters malicious commands in the blink of an eye. This script demonstrates why you should always lock your computer when away, and propely secure your ports.

### Usage instructions:
1. Edit the `Black-Widow.ino` file, changing the WebhookURL to either:<br>
    A. Your listening device's IP address and port;<br>
    B. Your URL on a webhook site like [webhook.site](https://webhook.site/).

2. Flash your Teensy with the edited `Black-Widow.ino` script.
3. *If you have decided to listen on your own device/server, launch the `capitan-hook.py` script on it.
4. Plug in the Teensy into the target Windows computer. Leave it plugged in until Teensy's LED starts flashing.
5. Your webhook should have received the network passwords.


##### This software is public domain. I do not take any responsibility for your actions.